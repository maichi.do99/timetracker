import { Task } from "../model/task";
import React from "react";
import axios from "axios";
import { useStateIfMounted } from "use-state-if-mounted";

export class TaskResponse {
  task?: Task;
  taskList?: Task[];
  error?: string;
}

type TaskContext = {
  taskList: Task[];
  taskById: Record<string, Task>;
  actions: {
    saveTask: (task: Task) => Promise<TaskResponse>;
    deleteTaskById: (id: string) => Promise<TaskResponse>;
    findTaskById: (id: string) => Promise<TaskResponse>;
    findAllTasks: () => void;
  };
};

const taskContext = React.createContext<TaskContext>({
  taskList: [],
  taskById: {},
  actions: {
    saveTask: async () => {
      return new TaskResponse();
    },
    deleteTaskById: async () => {
      return new TaskResponse();
    },
    findTaskById: async () => {
      return new TaskResponse();
    },
    findAllTasks: () => {
      return;
    },
  },
});

const url = "/api/task";

export const TaskProvider: React.FC = ({ children }) => {
  const [taskList, setTaskList] = useStateIfMounted<Task[]>([]);

  const findAll = async () => {
    const result = await axios.get(url);
    const response = result.data.taskList.map((item: any) => new Task(item));
    setTaskList(response);
  };

  const findById = async (id: string): Promise<TaskResponse> => {
    const result = await axios.get(url + "/" + id);
    const response = new TaskResponse();
    if (!result.data.error) {
      response.task = new Task(result.data.task);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const saveTask = async (task: Task): Promise<TaskResponse> => {
    const result = await axios.post(url, { task: task });
    const response = new TaskResponse();
    if (!result.data.error) {
      await findAll();
      response.task = new Task(result.data.savedTask);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const deleteTask = async (id: string) => {
    const result = await axios.delete(url + "/" + id);
    if (!result.data.error) {
      findAll();
    }
    return { error: result.data.error };
  };

  React.useEffect(() => {
    findAll();
  }, []);

  const taskById = taskList.reduce<Record<string, Task>>(
    (previous, current) => {
      previous[current.id] = current;
      return previous;
    },
    {}
  );

  return (
    <taskContext.Provider
      value={{
        taskById,
        taskList,
        actions: {
          saveTask: saveTask,
          deleteTaskById: deleteTask,
          findTaskById: findById,
          findAllTasks: findAll,
        },
      }}
    >
      {children}
    </taskContext.Provider>
  );
};

export function useTask() {
  return React.useContext(taskContext);
}
