import React, { useEffect } from "react";
import axios from "axios";
import { Timetracking } from "../model/timetracking";
import { useStateIfMounted } from "use-state-if-mounted";
import { Task } from "../model/task";

export class TimetrackingResponse {
  timetracking?: Timetracking;
  error?: string;
}

type TimetrackingContext = {
  timetrackingList: Timetracking[];
  timetrackingById: Record<string, Timetracking>;
  runningTimetrackingList: Timetracking[];
  actions: {
    findAllTimetrackingsByTaskId: (task: Task) => void;
    findAllRunningTimetrackings: () => void;
    findTimetrackingById: (id: string) => Promise<TimetrackingResponse>;
    saveTimetracking: (
      timetracking: Timetracking
    ) => Promise<TimetrackingResponse>;
    deleteTimetracking: (
      timetracking: Timetracking
    ) => Promise<TimetrackingResponse>;
    startTimetracking: (
      timtracking: Timetracking
    ) => Promise<TimetrackingResponse>;
    stopTimetracking: (
      timtracking: Timetracking
    ) => Promise<TimetrackingResponse>;
  };
};

const timetrackingContext = React.createContext<TimetrackingContext>({
  timetrackingList: [],
  timetrackingById: {},
  runningTimetrackingList: [],
  actions: {
    findAllTimetrackingsByTaskId: () => {
      return;
    },
    findAllRunningTimetrackings: () => {
      return;
    },
    findTimetrackingById: async () => {
      return new TimetrackingResponse();
    },
    saveTimetracking: async () => {
      return new TimetrackingResponse();
    },
    deleteTimetracking: async () => {
      return new TimetrackingResponse();
    },
    startTimetracking: async () => {
      return new TimetrackingResponse();
    },
    stopTimetracking: async () => {
      return new TimetrackingResponse();
    },
  },
});

const url = "/api/timetracking";

export const TimetrackingProvider: React.FC = ({ children }) => {
  const [timetrackingList, setTimetrackingList] = useStateIfMounted<
    Timetracking[]
  >([]);

  const [runningTimetrackingList, setRunningTimetrackingList] =
    useStateIfMounted<Timetracking[]>([]);

  useEffect(() => {
    findAllRunning();
  }, [timetrackingList]);

  const findAllByTaskId = async (task: Task) => {
    const result = await axios.get(url + "/task/" + task.id);
    const response = result.data.timetrackingList.map(
      (item: any) => new Timetracking(item)
    );
    setTimetrackingList(response);
  };

  const findAllRunning = async () => {
    const response = await axios.get(url + "/running");
    const result = response.data.timetrackingList.map(
      (item: any) => new Timetracking(item)
    );
    setRunningTimetrackingList(result);
  };

  const findById = async (id: string) => {
    const result = await axios.get(url + "/" + id);
    const response = new TimetrackingResponse();
    if (!result.data.error) {
      response.timetracking = new Timetracking(
        new Timetracking(result.data.timetracking)
      );
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const saveTimetracking = async (timetracking: Timetracking) => {
    const result = await axios.post(url, { timetracking: timetracking });
    const response = new TimetrackingResponse();
    if (!result.data.error) {
      response.timetracking = result.data.savedTimetracking;
      findAllByTaskId(timetracking.task);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const deleteTimetracking = async (timetracking: Timetracking) => {
    const result = await axios.delete(url + "/" + timetracking.id);
    if (!result.data.error) {
      findAllByTaskId(timetracking.task);
    }
    return { error: result.data.error };
  };

  const startTimetracking = async (timetracking: Timetracking) => {
    const result = await axios.post(url + "/start/" + timetracking.id);
    const response = new TimetrackingResponse();
    if (!result.data.error) {
      response.timetracking = result.data.savedTimetracking;
      findAllByTaskId(timetracking.task);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const stopTimetracking = async (timetracking: Timetracking) => {
    const result = await axios.post(url + "/stop/" + timetracking.id);
    const response = new TimetrackingResponse();
    if (!result.data.error) {
      response.timetracking = result.data.savedTimetracking;
      findAllByTaskId(timetracking.task);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const timetrackingById = timetrackingList.reduce<
    Record<string, Timetracking>
  >((previous, current) => {
    previous[current.id] = current;
    return previous;
  }, {});

  return (
    <timetrackingContext.Provider
      value={{
        timetrackingById: timetrackingById,
        timetrackingList: timetrackingList,
        runningTimetrackingList: runningTimetrackingList,
        actions: {
          findAllTimetrackingsByTaskId: findAllByTaskId,
          findAllRunningTimetrackings: findAllRunning,
          findTimetrackingById: findById,
          saveTimetracking: saveTimetracking,
          deleteTimetracking: deleteTimetracking,
          startTimetracking: startTimetracking,
          stopTimetracking: stopTimetracking,
        },
      }}
    >
      {children}
    </timetrackingContext.Provider>
  );
};

export function useTimetracking() {
  return React.useContext(timetrackingContext);
}
