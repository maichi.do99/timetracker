import React from "react";
import axios from "axios";
import { useStateIfMounted } from "use-state-if-mounted";
import { Label } from "../model/label";

export class LabelResponse {
  label?: Label;
  labelList?: Label[];
  error?: string;
}

type LabelContext = {
  labelList: Label[];
  labelById: Record<string, Label>;
  actions: {
    findAllLabels: () => void;
    findLabelById: (id: string) => Promise<LabelResponse>;
    saveLabel: (label: Label) => Promise<LabelResponse>;
    deleteLabel: (id: string) => Promise<LabelResponse>;
  };
};

const labelContext = React.createContext<LabelContext>({
  labelList: [],
  labelById: {},
  actions: {
    findAllLabels: () => {
      return;
    },
    findLabelById: async () => {
      return new LabelResponse();
    },
    saveLabel: async () => {
      return new LabelResponse();
    },
    deleteLabel: async () => {
      return new LabelResponse();
    },
  },
});

const url = "/api/label/";

export const LabelProvider: React.FC = ({ children }) => {
  const [labelList, setLabelList] = useStateIfMounted<Label[]>([]);

  const findAll = async () => {
    const response = await axios.get(url);
    const result = response.data.labelList.map((item: any) => new Label(item));
    setLabelList(result);
  };

  const findById = async (id: string) => {
    const result = await axios.get(url + id);
    const response = new LabelResponse();
    if (!result.data.error) {
      response.label = new Label(result.data.label);
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const saveLabel = async (label: Label) => {
    const result = await axios.post(url, { label: label });
    const response = new LabelResponse();
    if (!result.data.error) {
      response.label = result.data.savedLabel;
      await findAll();
    } else {
      response.error = result.data.error;
    }
    return response;
  };

  const deleteLabel = async (id: string) => {
    const result = await axios.delete(url + id);
    if (!result.data.error) {
      findAll();
    }
    return { error: result.data.error };
  };

  React.useEffect(() => {
    findAll();
  }, []);

  const labelById = labelList.reduce<Record<string, Label>>(
    (previous, current) => {
      previous[current.id] = current;
      return previous;
    },
    {}
  );

  return (
    <labelContext.Provider
      value={{
        labelById: labelById,
        labelList: labelList,
        actions: {
          findAllLabels: findAll,
          findLabelById: findById,
          saveLabel: saveLabel,
          deleteLabel: deleteLabel,
        },
      }}
    >
      {children}
    </labelContext.Provider>
  );
};

export function useLabel() {
  return React.useContext(labelContext);
}
