import * as React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import { TaskMasterDetail } from "./component/task/TaskMasterDetail";
import {
  AppBar,
  Button,
  createTheme,
  Grid,
  MuiThemeProvider,
  Toolbar,
} from "@material-ui/core";
import { LabelMasterDetail } from "./component/label/LabelMasterDetail";
import { TimetrackingProvider } from "./provider/TimetrackingProvider";
import { TaskProvider } from "./provider/TaskProvider";
import { LabelProvider } from "./provider/LabelProvider";

const theme = createTheme({
  palette: {
    primary: {
      main: "#000000",
    },
    secondary: {
      main: "#000000",
    },
  },
});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        style={{ backgroundColor: "#f7efed", height: "100vh" }}
      >
        <Grid item xs={7} style={{ backgroundColor: "white" }}>
          <TaskProvider>
            <LabelProvider>
              <TimetrackingProvider>
                <Router>
                  <AppBar
                    position={"static"}
                    style={{ backgroundColor: "#f0947f" }}
                  >
                    <Toolbar>
                      <Button>
                        <Link
                          style={{ color: "black", textDecoration: "none" }}
                          to="/"
                        >
                          Track it!
                        </Link>
                      </Button>
                      <Button>
                        <Link
                          style={{ color: "black", textDecoration: "none" }}
                          to="/task"
                        >
                          Task
                        </Link>
                      </Button>
                      <Button>
                        <Link
                          style={{ color: "black", textDecoration: "none" }}
                          to="/label"
                        >
                          Label
                        </Link>
                      </Button>
                    </Toolbar>
                  </AppBar>

                  <Switch>
                    <Route path="/task">
                      <TaskMasterDetail />
                    </Route>
                    <Route path="/label">
                      <LabelMasterDetail />
                    </Route>
                    <Route exact path="/">
                      <Redirect to="/task" />
                    </Route>
                  </Switch>
                </Router>
              </TimetrackingProvider>
            </LabelProvider>
          </TaskProvider>
        </Grid>
      </Grid>{" "}
    </MuiThemeProvider>
  );
}

export default App;
