export class CommonUtils {
  static isEmpty(s: string | any[]): boolean {
    return !s || s.length == 0;
  }

  static notEmpty(s: string | any[]): boolean {
    return !!s && s.length > 0;
  }
}
