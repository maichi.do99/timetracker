import { Box, Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import React from "react";

export function SaveButtonRow(props: {
  save: (close: boolean) => Promise<void>;
  close: () => void;
}) {
  return (
    <Box flexGrow={1}>
      <Button onClick={props.close}>
        <ArrowBack />
      </Button>
      <Button onClick={() => props.save(false)}>Speichern</Button>
      <Button onClick={() => props.save(true)}>Speichern & Schließen</Button>
    </Box>
  );
}
