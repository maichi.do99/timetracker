import React from "react";
import MuiAlert, { Color } from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";
import { CommonUtils } from "../../utils/CommonUtils";

export default function Alert(props: {
  alertType: Color;
  onClose: () => void;
  alertMessage: string;
}) {
  const handleClose = (reason: string) => {
    if (reason === "clickaway") {
      return;
    }
  };

  return (
    <Snackbar
      open={CommonUtils.notEmpty(props.alertMessage)}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      autoHideDuration={6000}
      onClose={(event: React.SyntheticEvent, reason: string) => {
        handleClose(reason);
        props.onClose();
      }}
    >
      <MuiAlert elevation={6} variant="filled" severity={props.alertType}>
        {props.alertMessage}
      </MuiAlert>
    </Snackbar>
  );
}
