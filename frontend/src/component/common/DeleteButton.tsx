import { Box, Button } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import React from "react";

export function DeleteButton(props: { delete: () => Promise<void> }) {
  return (
    <Box>
      <Button
        style={{
          backgroundColor: "#c71a1a",
          color: "#ffffff",
          padding: "8px 16px",
          borderRadius: 7,
        }}
        startIcon={<DeleteIcon />}
        onClick={props.delete}
      >
        Löschen
      </Button>
    </Box>
  );
}
