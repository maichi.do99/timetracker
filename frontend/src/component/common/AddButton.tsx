import { createStyles, Fab, makeStyles, Theme } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
    fab: {
      position: "absolute",
      right: "22vw",
      bottom: "5vh",
      "&:hover": {
        background: "#666666",
      },
    },
  })
);

export function AddButton(props: {
  label: string;
  disabled: boolean;
  create: () => void;
}) {
  const classes = useStyles();
  return (
    <Fab
      className={classes.fab}
      color="primary"
      aria-label="add"
      variant="extended"
      disabled={props.disabled}
      onClick={() => {
        props.create();
      }}
    >
      <AddIcon className={classes.extendedIcon} />
      {props.label}
    </Fab>
  );
}
