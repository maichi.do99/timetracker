import React from "react";
import { DataGrid, GridRowParams } from "@material-ui/data-grid";
import { useLabel } from "../../provider/LabelProvider";
import { AddButton } from "../common/AddButton";

type LabelListProps = {
  onDetail: (labelId: string | null) => void;
};

export function LabelList(props: LabelListProps) {
  const { labelList } = useLabel();

  function getColumns() {
    return [
      {
        field: "id",
        hide: true,
        editable: false,
      },
      {
        field: "labelName",
        headerName: "Label",
        type: "string",
        editable: false,
        width: 400,
      },
      {
        field: "tasks",
        headerName: "Anzahl Tasks",
        type: "number",
        editable: false,
        width: 200,
      },
    ];
  }

  function getRows() {
    return labelList.map((label) => {
      return {
        id: label.id,
        labelName: label.name,
        tasks: label.taskList.length,
      };
    });
  }

  function edit(rowParams: GridRowParams) {
    props.onDetail(rowParams.id.toString());
  }

  function create() {
    props.onDetail(null);
  }

  return (
    <div>
      <div style={{ padding: "3% 0% 1% 3%", fontSize: 24 }}>
        <b>Labelübersicht</b>
      </div>
      <div style={{ height: 400, width: "94%", margin: "2% 3% 0% 3%" }}>
        <DataGrid rows={getRows()} columns={getColumns()} onRowClick={edit} />
      </div>
      <AddButton label={"Label erstellen"} disabled={false} create={create} />
    </div>
  );
}
