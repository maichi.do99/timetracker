import React, { ChangeEvent } from "react";
import { useStateIfMounted } from "use-state-if-mounted";
import { CommonUtils } from "../../utils/CommonUtils";
import {
  Box,
  createStyles,
  List,
  ListItem,
  makeStyles,
  TextField,
} from "@material-ui/core";
import { Label } from "../../model/label";
import { useLabel } from "../../provider/LabelProvider";
import { DeleteButton } from "../common/DeleteButton";
import { SaveButtonRow } from "../common/SaveButtonRow";

type LabelDetailProps = {
  label: Label;
  onClose: () => void;
  onSuccess: (message: string) => void;
  onError: (error: string) => void;
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: 20,
    },
    box_1: {
      padding: 25,
      marginBottom: 20,
    },
    textfield: {
      width: "100%",
      marginBottom: 10,
      marginTop: 10,
    },
    heading: {
      paddingBottom: 10,
      paddingLeft: 10,
    },
  })
);

export function LabelDetail(props: LabelDetailProps) {
  const classes = useStyles();

  const {
    actions: { saveLabel, deleteLabel },
  } = useLabel();

  const [label, setLabel] = useStateIfMounted<Label>(props.label);

  async function save(close: boolean) {
    const response = await saveLabel(label);
    if (response.label) {
      setLabel(response.label);
      props.onSuccess("Das Label wurde erfolgreich gespeichert");
      if (close) {
        props.onClose();
      }
    } else if (response.error) {
      props.onError(response.error);
    }
  }

  async function doDelete() {
    const response = await deleteLabel(label.id);
    if (!response.error) {
      props.onSuccess("Das Label wurde erfolreich gelöscht");
      props.onClose();
    } else {
      props.onError(response.error);
    }
  }

  function setLabelName(event: ChangeEvent<HTMLInputElement>) {
    setLabel((old) => {
      const updatedLabel = { ...old };
      updatedLabel.name = event.target.value;
      return updatedLabel;
    });
  }

  return (
    <div className={classes.root}>
      <LabelDetails label={label} onChange={setLabelName} />
      <LabelTaskList label={label} />
      <Box display="flex">
        <SaveButtonRow close={props.onClose} save={save} />
        <DeleteButton delete={doDelete} />
      </Box>
    </div>
  );
}

function LabelDetails(props: {
  label: Label;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}) {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.heading}>Details</div>
      <Box className={classes.box_1} boxShadow={2} borderRadius={12}>
        <TextField
          className={classes.textfield}
          id="outlined-basic"
          label="Name"
          value={props.label.name}
          onChange={props.onChange}
        />
      </Box>
    </div>
  );
}

function LabelTaskList(props: { label: Label }) {
  const classes = useStyles();

  function getLabelTaskItemList() {
    if (CommonUtils.notEmpty(props.label.taskList)) {
      return props.label.taskList.map((task) => (
        <ListItem>{task.name}</ListItem>
      ));
    } else {
      return <div>Keine Tasks vorhanden</div>;
    }
  }

  return (
    <div>
      <div className={classes.heading}>Tasks</div>
      <Box className={classes.box_1} boxShadow={2} borderRadius={12}>
        <List>{getLabelTaskItemList()}</List>
      </Box>
    </div>
  );
}
