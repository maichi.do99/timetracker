import React, { useEffect } from "react";
import { useStateIfMounted } from "use-state-if-mounted";
import { useLabel } from "../../provider/LabelProvider";
import { LabelList } from "./LabelList";
import { LabelDetail } from "./LabelDetail";
import Alert from "../common/Alert";
import { Label } from "../../model/label";
import { Color } from "@material-ui/lab/Alert";

export function LabelMasterDetail() {
  const [display, setDisplay] = useStateIfMounted(
    <LabelList onDetail={showDetails} />
  );
  const [alertMessage, setAlertMessage] = useStateIfMounted<string>("");
  const [alertType, setAlertType] = useStateIfMounted<Color>("error");

  const {
    actions: { findAllLabels, findLabelById },
  } = useLabel();

  useEffect(() => {
    findAllLabels();
  }, [display]);

  async function showDetails(labelId: string | null) {
    if (labelId) {
      const response = await findLabelById(labelId);
      if (response.label) {
        setDisplay(
          <LabelDetail
            label={response.label}
            onClose={showList}
            onSuccess={setAlertSuccessMessage}
            onError={setAlertErrorMessage}
          />
        );
      } else if (response.error) {
        setAlertErrorMessage(response.error);
      }
    } else {
      setDisplay(
        <LabelDetail
          label={new Label()}
          onClose={showList}
          onSuccess={setAlertSuccessMessage}
          onError={setAlertErrorMessage}
        />
      );
    }
  }

  function showList() {
    findAllLabels();
    setDisplay(<LabelList onDetail={showDetails} />);
  }

  function setAlertSuccessMessage(message: string) {
    setAlertType("success");
    setAlertMessage(message);
  }

  function setAlertErrorMessage(error: string) {
    setAlertType("error");
    setAlertMessage(error);
  }

  return (
    <div>
      <Alert
        alertType={alertType}
        onClose={() => setAlertMessage("")}
        alertMessage={alertMessage}
      />
      <div>{display}</div>
    </div>
  );
}
