import { Task } from "../../model/task";
import { Box, createStyles, Grid, makeStyles } from "@material-ui/core";
import { TaskResponse, useTask } from "../../provider/TaskProvider";
import React, { useEffect } from "react";
import { TaskLabelList } from "./label/TaskLabelList";
import { useStateIfMounted } from "use-state-if-mounted";
import { TimetrackingList } from "./timetracking/TimetrackingList";
import { Label } from "../../model/label";
import { TaskTextField } from "./components/TaskTextField";
import { DeleteButton } from "../common/DeleteButton";
import { SaveButtonRow } from "../common/SaveButtonRow";

type TaskDetailProps = {
  task: Task;
  onClose: () => void;
  onSuccess: (message: string) => void;
  onError: (error: string) => void;
};

export enum FilterType {
  R,
  S,
  E,
}

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: 20,
    },
    box_1: {
      height: "18vh",
      maxHeight: "18vh",
      overflow: "auto",
      padding: 25,
      marginBottom: 20,
    },
    box_2: {
      maxHeight: "30vh",
      overflow: "auto",
      padding: 25,
      marginBottom: 20,
    },
    heading: {
      paddingBottom: 10,
      paddingLeft: 10,
    },
  })
);

export function TaskDetail(props: TaskDetailProps) {
  const classes = useStyles();
  const [task, setTask] = useStateIfMounted<Task>(props.task);

  const {
    actions: { saveTask, deleteTaskById },
  } = useTask();

  useEffect(() => {
    setTask(props.task);
  }, [props.task]);

  async function save(close: boolean) {
    const response: TaskResponse = await saveTask(task);

    if (response.task) {
      setTask(response.task);
      props.onSuccess("Der Task wurde erfolgreich gespeichert.");
    } else if (response.error) {
      props.onError(response.error);
    }

    if (close) {
      props.onClose();
    }
  }

  async function doDelete() {
    const response: TaskResponse = await deleteTaskById(task.id);

    if (!response.error) {
      props.onSuccess("Der Task wurde erfolgreich gelöscht.");
      props.onClose();
    } else {
      props.onError(response.error);
    }
  }

  function setDescription(event: React.ChangeEvent<HTMLInputElement>) {
    setTask((old) => {
      const updated = { ...old };
      updated.description = event.target.value;
      return updated;
    });
  }

  function setName(event: React.ChangeEvent<HTMLInputElement>) {
    setTask((old) => {
      const updated = { ...old };
      updated.name = event.target.value;
      return updated;
    });
  }

  function setLabelList(labelList: Label[]) {
    setTask((old) => {
      const updatedTask = { ...old };
      updatedTask.labelList = labelList;
      return updatedTask;
    });
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <div className={classes.heading}>Details</div>
          <Box className={classes.box_1} boxShadow={2} borderRadius={12}>
            <TaskTextField
              label="Name"
              value={task.name ? task.name : ""}
              onChange={setName}
            />
            <TaskTextField
              label="Beschreibung"
              value={task.description ? task.description : ""}
              onChange={setDescription}
            />
          </Box>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.heading}>Labels</div>
          <Box className={classes.box_1} boxShadow={2} borderRadius={12}>
            <TaskLabelList
              labelList={task.labelList}
              onChange={setLabelList}
              onSuccess={props.onSuccess}
              onError={props.onError}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <TimetrackingList task={task} />
        </Grid>
      </Grid>
      <Box display="flex">
        <SaveButtonRow save={save} close={props.onClose} />
        <DeleteButton delete={doDelete} />
      </Box>
    </div>
  );
}
