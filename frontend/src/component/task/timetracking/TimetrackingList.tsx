import React, { useEffect } from "react";
import { useStateIfMounted } from "use-state-if-mounted";
import { Box, createStyles, List, makeStyles, Theme } from "@material-ui/core";
import { CommonUtils } from "../../../utils/CommonUtils";
import Alert from "../../common/Alert";
import { AddButton } from "../../common/AddButton";
import { Timetracking } from "../../../model/timetracking";
import { Task } from "../../../model/task";
import { useTimetracking } from "../../../provider/TimetrackingProvider";
import { FilterType } from "../TaskDetail";
import TimetrackingDetail from "./TimetrackingDetail";
import { TimetrackingFilter } from "./components/TimetrackingFilter";
import { TimetrackingListItem } from "./components/TimetrackingListItem";
import { Color } from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    box: {
      maxHeight: "30vh",
      overflow: "auto",
      padding: 25,
      marginBottom: 20,
    },
    heading: {
      paddingBottom: 10,
      paddingLeft: 10,
    },
    checkbox: {
      marginRight: theme.spacing(2),
    },
  })
);

type TimetrackingListProps = {
  task: Task;
};

export function TimetrackingList(props: TimetrackingListProps) {
  const classes = useStyles();
  const [task, setTask] = useStateIfMounted<Task>(props.task);
  const {
    timetrackingList,
    actions: {
      findAllTimetrackingsByTaskId,
      findTimetrackingById,
      deleteTimetracking,
      startTimetracking,
      stopTimetracking,
    },
  } = useTimetracking();

  const [editTimetracking, setEditTimetracking] =
    useStateIfMounted<Timetracking>(new Timetracking());
  const [timetrackingFilter, setTimetrackingFilter] = useStateIfMounted<
    FilterType[]
  >([FilterType.R, FilterType.S]);
  const [openDetail, setOpenDetail] = useStateIfMounted<boolean>(false);
  const [alertMessage, setAlertMessage] = useStateIfMounted<string>("");
  const [alertType, setAlertType] = useStateIfMounted<Color>("error");

  useEffect(() => {
    setTask(props.task);
    findAllTimetrackingsByTaskId(props.task);
  }, [props.task]);

  async function showDetails(timetracking: Timetracking) {
    const response = await findTimetrackingById(timetracking.id);
    if (response.timetracking) {
      setEditTimetracking(response.timetracking);
      setOpenDetail(true);
    } else if (response.error) {
      setAlertType("error");
      setAlertMessage(response.error);
    }
  }

  function closeDetails() {
    setEditTimetracking(new Timetracking());
    setOpenDetail(false);
  }

  async function handleTimingClick(timetracking: Timetracking) {
    let response;
    let action: string;
    if (timetracking.startTime) {
      if (timetracking.endTime) {
        response = await deleteTimetracking(timetracking);
        action = "gelöscht";
      } else {
        response = await stopTimetracking(timetracking);
        action = "gestoppt";
      }
    } else {
      response = await startTimetracking(timetracking);
      action = "gestartet";
    }
    if (response.error) {
      setAlertErrorMessage(response.error);
    } else {
      setAlertSuccessMessage(
        "Das Timetracking wurde erfolgreich " + action + "."
      );
    }
  }

  function createTimetracking() {
    const timetracking = new Timetracking();
    timetracking.task = props.task;
    setEditTimetracking(timetracking);
    setOpenDetail(true);
  }

  function addFilter(filterType: FilterType) {
    setTimetrackingFilter((filterList) => filterList.concat(filterType));
  }

  function removeFilter(filterType: FilterType) {
    setTimetrackingFilter((filterList) =>
      filterList.filter((type) => type != filterType)
    );
  }

  function setAlertSuccessMessage(message: string) {
    setAlertType("success");
    setAlertMessage(message);
  }

  function setAlertErrorMessage(error: string) {
    setAlertType("error");
    setAlertMessage(error);
  }

  return (
    <div>
      <Alert
        alertType={alertType}
        onClose={() => setAlertMessage("")}
        alertMessage={alertMessage}
      />
      <div className={classes.heading}>Timetrackings</div>
      <TimetrackingFilter
        filter={timetrackingFilter}
        add={addFilter}
        remove={removeFilter}
      />
      <TrackingList
        task={task}
        filter={timetrackingFilter}
        timetrackingList={timetrackingList}
        handleTimingClick={handleTimingClick}
        showDetails={showDetails}
      />
      <AddButton
        label={"Timetracking erstellen"}
        disabled={!task.id}
        create={createTimetracking}
      />
      <TimetrackingDetail
        open={openDetail}
        timetracking={editTimetracking}
        onClose={closeDetails}
        onSuccess={setAlertSuccessMessage}
        onError={setAlertErrorMessage}
      />
    </div>
  );
}

function TrackingList(props: {
  task: Task;
  filter: FilterType[];
  timetrackingList: Timetracking[];
  showDetails: (timetracking: Timetracking) => void;
  handleTimingClick: (timetracking: Timetracking) => void;
}) {
  const classes = useStyles();

  const filteredTimetrackingList = props.timetrackingList.filter(
    (timetracking) => filterTimetrackingList(props.filter, timetracking)
  );

  function getTrackingItemList() {
    if (CommonUtils.notEmpty(filteredTimetrackingList)) {
      return props.timetrackingList
        .filter((timetracking) =>
          filterTimetrackingList(props.filter, timetracking)
        )
        .map((timetracking) => (
          <TimetrackingListItem
            timetracking={timetracking}
            showDetails={props.showDetails}
            handleTimingClick={props.handleTimingClick}
          />
        ));
    } else {
      return <div>Keine Timetrackings</div>;
    }
  }

  if (props.task.id) {
    return (
      <Box className={classes.box} boxShadow={2} borderRadius={12}>
        <List> {getTrackingItemList()}</List>
      </Box>
    );
  } else {
    return (
      <Box className={classes.box} boxShadow={2} borderRadius={12}>
        <div>
          Der Task muss vorerst angelegt werden. Bitte speichere den Task.
        </div>
      </Box>
    );
  }
}

function filterTimetrackingList(
  timetrackingFilter: FilterType[],
  tracking: Timetracking
): boolean {
  if (tracking) {
    if (timetrackingFilter.includes(FilterType.R)) {
      if (tracking.startTime !== null && tracking.endTime === null) {
        return true;
      }
    }
    if (timetrackingFilter.includes(FilterType.S)) {
      if (tracking.startTime === null && tracking.endTime === null) {
        return true;
      }
    }
    if (timetrackingFilter.includes(FilterType.E)) {
      if (tracking.startTime !== null && tracking.endTime !== null) {
        return true;
      }
    }
  }

  return false;
}
