import React, { ChangeEvent } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from "@material-ui/core";
import { Timetracking } from "../../../model/timetracking";
import {
  TimetrackingResponse,
  useTimetracking,
} from "../../../provider/TimetrackingProvider";
import { useStateIfMounted } from "use-state-if-mounted";
import { DateTextField } from "./components/DateTextField";
import { DescriptionTextField } from "./components/DescriptionTextField";

type TimetrackingDetailProps = {
  timetracking: Timetracking;
  onClose: () => void;
  onSuccess: (message: string) => void;
  onError: (error: string) => void;
  open: boolean;
};

export enum DateType {
  END,
  START,
}

export default function TimetrackingDetail(props: TimetrackingDetailProps) {
  const {
    actions: { saveTimetracking },
  } = useTimetracking();

  const [timetracking, setTimetracking] = useStateIfMounted<Timetracking>(
    props.timetracking
  );

  React.useEffect(() => {
    setTimetracking(props.timetracking);
  }, [props.timetracking]);

  async function save() {
    const response: TimetrackingResponse = await saveTimetracking(timetracking);
    if (!response.error) {
      props.onClose();
      props.onSuccess("Das Timetracking wurde erfolgreich gespeichert.");
    } else {
      props.onError(response.error);
    }
  }

  function setDescription(event: React.ChangeEvent<HTMLInputElement>) {
    setTimetracking((old) => {
      const updatedTimetracking = { ...old };
      updatedTimetracking.description = event.target.value;
      return updatedTimetracking;
    });
  }

  function updateStartTime(event: ChangeEvent<HTMLInputElement>) {
    setTimetracking((oldTracking) => {
      const updatedTracking = { ...oldTracking };
      updatedTracking.startTime = new Date(event.target.value);
      return updatedTracking;
    });
  }

  function updateEndTime(event: ChangeEvent<HTMLInputElement>) {
    setTimetracking((oldTracking) => {
      const updatedTracking = { ...oldTracking };
      updatedTracking.endTime = new Date(event.target.value);
      return updatedTracking;
    });
  }

  return (
    <div>
      <Dialog open={props.open} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          Timetracking {props.timetracking.id ? "bearbeiten" : "erstellen"}
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <DescriptionTextField
                timetracking={timetracking}
                setDescription={setDescription}
              />
            </Grid>
            <Grid item xs={6}>
              <DateTextField
                dateType={DateType.START}
                time={timetracking.startTime}
                onChange={updateStartTime}
              />
            </Grid>
            <Grid item xs={6}>
              <DateTextField
                dateType={DateType.END}
                time={timetracking.endTime}
                onChange={updateEndTime}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose} color="primary">
            Schließen
          </Button>
          <Button onClick={save} color="primary">
            Speichern
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
