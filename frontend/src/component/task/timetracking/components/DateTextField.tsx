import React, { ChangeEvent } from "react";
import Moment from "moment-timezone";
import { TextField } from "@material-ui/core";
import { DateType } from "../TimetrackingDetail";

export function DateTextField(props: {
  dateType: DateType;
  time: Date;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}) {
  const value = props.time ? Moment(props.time).format("yyy-MM-DDTHH:mm") : "";
  const label = props.dateType === DateType.START ? "Starttime" : "Endtime";

  return (
    <TextField
      id="datetime-local"
      label={label}
      type="datetime-local"
      disabled={!props.time}
      value={value}
      onChange={props.onChange}
      InputLabelProps={{
        shrink: true,
      }}
      fullWidth
    />
  );
}
