import { Timetracking } from "../../../../model/timetracking";
import React, { ChangeEvent } from "react";
import { TextField } from "@material-ui/core";

export function DescriptionTextField(props: {
  timetracking: Timetracking;
  setDescription: (event: ChangeEvent<HTMLInputElement>) => void;
}) {
  return (
    <TextField
      autoFocus
      id="description"
      label="Beschreibung"
      type="text"
      value={props.timetracking.description}
      onChange={props.setDescription}
      fullWidth
    />
  );
}
