import { FilterType } from "../../TaskDetail";
import React, { ChangeEvent } from "react";
import { Checkbox, createStyles, makeStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    filter: {
      display: "flex",
      flexDirection: "row",
      flex: "start",
      padding: 0,
    },
    filter_item: {
      display: "flex",
      flexDirection: "row",
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    checkbox: {
      color: "primary",
      "&$checked": {
        color: "primary",
      },
    },
  })
);

export function TimetrackingFilter(props: {
  filter: FilterType[];
  add: (filterType: FilterType) => void;
  remove: (filterType: FilterType) => void;
}) {
  function handleFilterChange(
    event: ChangeEvent<HTMLInputElement>,
    type: FilterType
  ) {
    if (event.target.checked) {
      props.add(type);
    } else {
      props.remove(type);
    }
  }
  const classes = useStyles();

  return (
    <div className={classes.filter}>
      <div className={classes.filter_item}>
        <Checkbox
          className={classes.checkbox}
          edge="start"
          onChange={(event) => handleFilterChange(event, FilterType.R)}
          checked={props.filter.includes(FilterType.R)}
        />
        <p>Laufend</p>
      </div>
      <div className={classes.filter_item}>
        <Checkbox
          className={classes.checkbox}
          edge="start"
          onChange={(event) => handleFilterChange(event, FilterType.S)}
          checked={props.filter.includes(FilterType.S)}
        />
        <p>Noch nicht gestartet</p>
      </div>
      <div className={classes.filter_item}>
        <Checkbox
          className={classes.checkbox}
          edge="start"
          onChange={(event) => handleFilterChange(event, FilterType.E)}
          checked={props.filter.includes(FilterType.E)}
        />
        <p>Beendet</p>
      </div>
    </div>
  );
}
