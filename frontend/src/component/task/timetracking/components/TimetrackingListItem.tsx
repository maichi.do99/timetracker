import { Timetracking } from "../../../../model/timetracking";
import Moment from "moment";
import { Delete, PlayArrow, Stop } from "@material-ui/icons";
import {
  IconButton,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@material-ui/core";
import React from "react";

export function TimetrackingListItem(props: {
  timetracking: Timetracking;
  showDetails: (timetracking: Timetracking) => void;
  handleTimingClick: (timetracking: Timetracking) => void;
}) {
  function getStartTimeText() {
    if (props.timetracking.startTime) {
      return Moment(props.timetracking.startTime).format("DD.MM.yyyy HH:mm");
    } else {
      return "-";
    }
  }

  function getEndTimeText() {
    if (props.timetracking.endTime) {
      return Moment(props.timetracking.endTime).format("DD.MM.yyyy HH:mm");
    } else {
      return "-";
    }
  }

  function getTimingActionButton() {
    if (props.timetracking.startTime) {
      if (props.timetracking.endTime) {
        return <Delete />;
      } else {
        return <Stop />;
      }
    } else {
      return <PlayArrow />;
    }
  }

  return (
    <div>
      <ListItem
        onClick={() => {
          props.showDetails(props.timetracking);
        }}
      >
        <ListItemText
          primary={props.timetracking.description}
          secondary={
            <div>
              <p style={{ paddingRight: "20px", display: "inline" }}>
                Start: {getStartTimeText()}
              </p>
              <p style={{ display: "inline" }}>End: {getEndTimeText()}</p>
            </div>
          }
        />
        <ListItemSecondaryAction>
          <IconButton
            edge="end"
            aria-label="start"
            onClick={() => {
              props.handleTimingClick(props.timetracking);
            }}
          >
            {getTimingActionButton()}
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    </div>
  );
}
