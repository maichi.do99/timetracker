import React, { ChangeEventHandler } from "react";
import { createStyles, makeStyles, TextField } from "@material-ui/core";

const useStyles = makeStyles(() =>
  createStyles({
    textfield: {
      width: "100%",
      marginBottom: 10,
      marginTop: 10,
    },
  })
);

export function TaskTextField(props: {
  label: string;
  value: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
}) {
  const classes = useStyles();
  return (
    <TextField
      className={classes.textfield}
      id="outlined-basic"
      label={props.label}
      value={props.value}
      onChange={props.onChange}
    />
  );
}
