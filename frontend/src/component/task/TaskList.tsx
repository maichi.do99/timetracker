import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { useTask } from "../../provider/TaskProvider";
import { DataGrid } from "@material-ui/data-grid";
import { AddButton } from "../common/AddButton";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
  })
);

type TaskListProps = {
  onDetail: (taskId: string | null) => void;
};

export function TaskList(props: TaskListProps) {
  const classes = useStyles();

  const { taskList } = useTask();

  function getColumns() {
    return [
      {
        field: "id",
        hide: true,
        editable: false,
      },
      {
        field: "taskName",
        headerName: "Task",
        type: "string",
        editable: false,
        width: 250,
      },
      {
        field: "taskDescription",
        headerName: "Beschreibung",
        type: "string",
        editable: false,
        width: 250,
      },
      {
        field: "labels",
        headerName: "Labels",
        type: "string",
        editable: false,
        width: 250,
      },
      {
        field: "runningTimetrackings",
        headerName: "laufende Trackings",
        type: "number",
        editable: false,
        width: 150,
      },
      {
        field: "timetrackings",
        headerName: "Anzahl Trackings",
        type: "number",
        editable: false,
        width: 150,
      },
    ];
  }

  function getRows() {
    return taskList.map((task) => {
      return {
        id: task.id,
        taskName: task.name,
        taskDescription: task.description,
        labels: task.labelList.map((label) => label.name).join(", "),
        timetrackings: task.timetrackingList.length,
        runningTimetrackings: task.timetrackingList.filter(
          (tracking) => tracking.startTime && !tracking.endTime
        ).length,
      };
    });
  }

  return (
    <div className={classes.root}>
      <div style={{ padding: "3% 0% 1% 3%", fontSize: 20 }}>
        <b>Taskübersicht</b>
      </div>
      <div style={{ height: 400, width: "94%", margin: "2% 3% 0% 3%" }}>
        <DataGrid
          rows={getRows()}
          columns={getColumns()}
          onRowClick={(rowParams) => {
            props.onDetail(rowParams.id.toString());
          }}
        />
      </div>
      <AddButton
        label={"Task erstellen"}
        disabled={false}
        create={() => props.onDetail(null)}
      />
    </div>
  );
}
