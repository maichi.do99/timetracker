import React, { useEffect } from "react";
import { TaskList } from "./TaskList";
import { TaskDetail } from "./TaskDetail";
import { TaskResponse, useTask } from "../../provider/TaskProvider";
import { useStateIfMounted } from "use-state-if-mounted";
import { Task } from "../../model/task";
import { useTimetracking } from "../../provider/TimetrackingProvider";
import Alert from "../common/Alert";
import { Color } from "@material-ui/lab/Alert";

export function TaskMasterDetail() {
  const [display, setDisplay] = useStateIfMounted(
    <TaskList onDetail={showDetail} />
  );
  const [alertMessage, setAlertMessage] = useStateIfMounted<string>("");
  const [alertType, setAlertType] = useStateIfMounted<Color>("error");
  const {
    actions: { findAllTasks, findTaskById },
  } = useTask();

  const {
    actions: { findAllTimetrackingsByTaskId },
  } = useTimetracking();

  useEffect(() => {
    findAllTasks();
  }, [display]);

  async function showDetail(taskId: string | null) {
    const task = await loadTaskDetail(taskId);
    setDisplay(
      <TaskDetail
        task={task}
        onClose={showList}
        onSuccess={setAlertSuccessMessage}
        onError={setAlertErrorMessage}
      />
    );
  }

  async function loadTaskDetail(taskId: string | null) {
    let task = new Task();
    if (taskId) {
      const response: TaskResponse = await findTaskById(taskId);
      if (!response.error && response.task) {
        task = response.task;
        findAllTimetrackingsByTaskId(task);
      } else if (response.error) {
        setAlertMessage(response.error);
      }
    }
    return task;
  }

  function showList() {
    findAllTasks();
    setDisplay(<TaskList onDetail={showDetail} />);
  }

  function setAlertSuccessMessage(message: string) {
    setAlertType("success");
    setAlertMessage(message);
  }

  function setAlertErrorMessage(error: string) {
    setAlertType("error");
    setAlertMessage(error);
  }

  return (
    <div>
      <Alert
        alertType={alertType}
        onClose={() => setAlertMessage("")}
        alertMessage={alertMessage}
      />
      <div>{display}</div>
    </div>
  );
}
