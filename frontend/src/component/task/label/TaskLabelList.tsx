import { useLabel } from "../../../provider/LabelProvider";
import { Label } from "../../../model/label";
import React, { ChangeEvent, useEffect } from "react";
import { FormControl, FormGroup } from "@material-ui/core";
import { useStateIfMounted } from "use-state-if-mounted";
import { AddLabelTextField } from "./components/AddLabelTextField";
import { LabelListItem } from "./components/LabelListItem";

type TaskLabelListProps = {
  labelList: Label[];
  onChange: (labelList: Label[]) => void;
  onSuccess: (message: string) => void;
  onError: (error: string) => void;
};

export function TaskLabelList(props: TaskLabelListProps) {
  const {
    labelList,
    actions: { saveLabel },
  } = useLabel();

  const [taskLabelList, setTaskLabelList] = useStateIfMounted<Label[]>([
    ...props.labelList,
  ]);

  useEffect(() => {
    setTaskLabelList(props.labelList);
  }, [props.labelList]);

  function addLabelToTask(label: Label) {
    setTaskLabelList((oldList) => {
      const updatedList = oldList.concat(label);
      props.onChange(updatedList);
      return updatedList;
    });
  }

  function deleteLabelFromTask(label: Label) {
    setTaskLabelList((oldList) => {
      const updatedList = oldList.filter((item) => item.id != label.id);
      props.onChange(updatedList);
      return updatedList;
    });
  }

  function handleLabelCheckboxChange(
    event: ChangeEvent<HTMLInputElement>,
    label: Label
  ) {
    event.target.checked ? addLabelToTask(label) : deleteLabelFromTask(label);
  }

  async function createLabel(name: string): Promise<boolean> {
    let label = new Label();
    label.name = name;
    const response = await saveLabel(label);
    if (response.label) {
      label = response.label;
      addLabelToTask(label);
      props.onSuccess("Das Label wurde erfolgreich erstellt.");
      return true;
    } else if (response.error) {
      props.onError(response.error);
    }
    return false;
  }

  return (
    <div>
      <AddLabelTextField createLabel={createLabel} />
      <FormControl component="fieldset">
        <FormGroup>
          {labelList.map((label) => (
            <LabelListItem
              label={label}
              onChange={handleLabelCheckboxChange}
              taskLabelList={taskLabelList}
            />
          ))}
        </FormGroup>
      </FormControl>
    </div>
  );
}
