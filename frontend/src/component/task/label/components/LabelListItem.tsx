import { Label } from "../../../../model/label";
import React, { ChangeEvent } from "react";
import {
  Checkbox,
  createStyles,
  FormControlLabel,
  makeStyles,
  Theme,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    checkbox: {
      marginRight: theme.spacing(2),
      color: "primary",
      "&$checked": {
        color: "primary",
      },
    },
  })
);

export function LabelListItem(props: {
  label: Label;
  onChange: (event: ChangeEvent<HTMLInputElement>, label: Label) => void;
  taskLabelList: Label[];
}) {
  const classes = useStyles();
  return (
    <FormControlLabel
      label={props.label.name}
      control={
        <Checkbox
          className={classes.checkbox}
          edge="end"
          onChange={(event) => props.onChange(event, props.label)}
          checked={props.taskLabelList
            .map((l) => l.id)
            .includes(props.label.id)}
        />
      }
    />
  );
}
