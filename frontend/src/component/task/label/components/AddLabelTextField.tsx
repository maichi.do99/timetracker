import { useStateIfMounted } from "use-state-if-mounted";
import { IconButton, InputAdornment, TextField } from "@material-ui/core";
import { CommonUtils } from "../../../../utils/CommonUtils";
import AddIcon from "@material-ui/icons/Add";
import React from "react";

export function AddLabelTextField(props: {
  createLabel: (name: string) => Promise<boolean>;
}) {
  const [addLabel, setAddLabel] = useStateIfMounted<string>("");

  async function createLabel() {
    const createdSuccessfully = await props.createLabel(addLabel);
    if (createdSuccessfully) {
      setAddLabel("");
    }
  }

  return (
    <TextField
      id="outlined-basic"
      label="Label erstellen"
      value={addLabel}
      onKeyPress={(e) => {
        if (e.key === "Enter") {
          createLabel();
        }
      }}
      onChange={(event) => {
        setAddLabel(event.target.value);
      }}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              disabled={CommonUtils.isEmpty(addLabel)}
              onClick={createLabel}
            >
              <AddIcon />
            </IconButton>
          </InputAdornment>
        ),
      }}
      fullWidth
    />
  );
}
