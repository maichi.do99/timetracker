import { BaseEntity } from "./BaseEntity";
import { Label } from "./label";
import { Timetracking } from "./timetracking";

export class Task extends BaseEntity {
  name!: string;
  description: string | undefined;
  labelList: Label[] = [];
  timetrackingList: Timetracking[] = [];

  constructor(json?: any) {
    super(json);
    if (json) {
      this.name = json.name;
      this.description = json.description;
      this.labelList = json.labelList;
      this.timetrackingList = json.timetrackingList;
    }
  }
}
