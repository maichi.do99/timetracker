import { BaseEntity } from "./BaseEntity";
import { Task } from "./task";

export class Timetracking extends BaseEntity {
  description!: string;
  startTime!: Date;
  endTime!: Date;
  task!: Task;

  constructor(json?: any) {
    super(json);
    if (json) {
      this.description = json.description;
      this.startTime = json.startTime;
      this.endTime = json.endTime;
      this.task = new Task(json.task);
    }
  }
}
