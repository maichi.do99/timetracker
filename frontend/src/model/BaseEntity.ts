export class BaseEntity {
  id!: string;
  createdAt!: Date;
  updatedAt!: Date;

  constructor(json?: any) {
    if (json) {
      this.id = json.id;
      this.createdAt = json.createdAt;
      this.updatedAt = json.updatedAt;
    }
  }
}
