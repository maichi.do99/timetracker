import { BaseEntity } from "./BaseEntity";
import { Task } from "./task";

export class Label extends BaseEntity {
  name!: string;
  taskList: Task[] = [];

  constructor(json?: any) {
    super(json);
    if (json) {
      this.name = json.name;
      this.taskList = json.taskList;
    }
  }
}
