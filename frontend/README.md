# Track It! - Dein Timetracker (React Frontend)

Mit Track it! wird das tracken von Aufgaben und Tasks kinderleicht gemacht. Lege einen Task an und du kannst sofort dein erstes Tracking starten!
Es handelt sich hierbei um das Frontend, das die über die Library React die Daten visuell anzeigen kann.

## Genutzte Technologien & Requirements

- NodeJs
- React (Typescript)
- Docker

## Funktionalität

Der Track it! Timetracker besteht aus dem Hauptdashboard, auf der die Taskübersicht dargestellt ist, und der Labelübersicht.

Tasks beschreiben hierbei eine Aufgabe, zu der beliebig viele Timetrackings hinzugefügt werden können. Um den Task genauer beschreiben zu können, ist es neben einer Textbeschreibung möglich, Labels zuzuordnen. Auf dem Dashboard lassen sich verschiedene Filter zur Findung eines Tasks einstellen. So kann beispielweise nach Tasks gefiltert werden, die ein bestimmtes Label beinhalten.

Labels können bereits vor Erstellung eines Tasks erstellt werden und anschließend zugewiesen werden oder direkt aus einem Task heraus erstellt werden. Auf der Detailseite eines Labels kann zudem eingesehen werden, in welchen Tasks sie verwendet werden.

Timetrackings werden für jeweils einen Task erstellt und müssen dann manuell gestartet werden. Dabei wird der Startzeitpunkt für diesen festgehalten. Wird das Tracking gestoppt, wird auch die Endzeit festgehalten. Das Timetracking gilt somit als "Beendet" und kann nun gelöscht oder in Start- und Endzeit bearbeitet werden. Auch Timetrackings lassen sich filtern, jedoch nur danach, ob sie derzeit laufen, noch nicht gestartet wurden oder bereits abgeschlossen und damit "beendet" wurden.

## Setup

1. **Clone** das **Repository** mithilfe von Git.
2. **Öffne** das Projekt in einer beliebigen IDE. Bevorzugt **IntelliJ**, da das Setup sich daran anlehnt. Solltest du jedoch kein IntelliJ verwenden, müssen noch einige Config-Dateien abgeändert werden, um einen Start des Front- und Backends über Docker ermöglichen zu können.
3. Starte das **Docker-Compose** File unter dem Hauptordner.
4. **Installiere** die **Dependencies** für die jeweiligen beiden Node Projekte unter den Ordnern "backend" und "frontend" mithilfe des Befehls "**npm install**".
5. Füge die **Datenbank zu IntelliJ** hinzu: (Achtung! Achte darauf, dass die Datenbank unter Docker gestartet wurde)
   5.1. Öffne auf der rechten Seite den Tab "Database".
   5.2. Füge eine neue Datenbank hinzu und wähle als Datasource die Option "MySQL" aus.
   5.3. Der Host lautet "localhost", der Port "3306". User und Passwort findest du in der ormconfig.js Datei unter dem Punkt "config".
   5.4. Nachdem du die Treiber heruntergeladen hast, kannst du die Verbindung zur Datenbank testen.
6. Führe das Skript **"schema:sync" in der backend/package.json** aus, um das Datenbank Schema zu synchronisieren.
7. Um nun das Backend zu starten, starte das **"start" Skript in der backend/package.json**.
8. Um nun das Frontend zu starten, starte das **"start" Skript in der frontend/package.json**.
9. Öffne nun den Timetracker in deinem Browser unter der Adresse "**http://localhost:3000**".

## Testen der Anwendung

Folgende Funktionalitäten können im Frontend getestet werden:

Tasks:
- Erstellen eines Tasks
- Bearbeiten eines Tasks
- Löschen eines Tasks

Timetrackings
- Erstellen eines Timetrackings innerhalb eines Tasks
- Bearbeiten eines Timetrackings
- Löschen eines Timetrackings
- Starten eines Timetrackings
- Beenden eines Timetrackings
- Filtern der Timetrackings

Labels

- Erstellen eines Labels
- Bearbeiten eines Labels
- Löschen eines Labels
- Erstellen eines Labels innerhalb eines Tasks

Zu allen Funktionalitäten kann ebenfalls ein nicht erfolgreiches Speichern getestet werden. Der Nutzer wird dann darauf aufmerksam gemacht.