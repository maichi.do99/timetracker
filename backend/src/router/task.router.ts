import { Router } from "express";
import {
  addLabelListToTask,
  deleteLabelListFromTask,
  deleteTask,
  findAllTasks,
  findTaskById,
  findAllByLabelId,
  saveTask,
} from "../controller/task.controller";

export const taskRouter = Router({ mergeParams: true });

taskRouter.get("/", findAllTasks);
taskRouter.get("/:id", findTaskById);
taskRouter.post("/", saveTask);
taskRouter.delete("/:id", deleteTask);
taskRouter.put("/labels/:id", addLabelListToTask);
taskRouter.delete("/labels/:id", deleteLabelListFromTask);
taskRouter.get("/labels/:labelId", findAllByLabelId);
