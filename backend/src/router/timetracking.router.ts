import { Router } from "express";
import {
  saveTimetracking,
  findAllTimetrackings,
  deleteTimetracking,
  startTimetracking,
  stopTimetracking,
  findAllByTaskId,
  findTimetrackingById,
  findAllRunning,
} from "../controller/timetracking.controller";

export const timetrackingRouter = Router({ mergeParams: true });

timetrackingRouter.get("/running", findAllRunning);
timetrackingRouter.get("/", findAllTimetrackings);
timetrackingRouter.get("/:id", findTimetrackingById);
timetrackingRouter.get("/task/:id", findAllByTaskId);
timetrackingRouter.post("/", saveTimetracking);
timetrackingRouter.delete("/:id", deleteTimetracking);
timetrackingRouter.post("/start/:id", startTimetracking);
timetrackingRouter.post("/stop/:id", stopTimetracking);
