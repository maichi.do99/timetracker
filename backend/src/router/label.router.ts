import { Router } from "express";
import {
  deleteLabel,
  findAllLabels,
  findLabelById,
  saveLabel,
} from "../controller/label.controller";

export const labelRouter = Router({ mergeParams: true });
labelRouter.get("/", findAllLabels);
labelRouter.get("/:id", findLabelById);
labelRouter.post("/", saveLabel);
labelRouter.delete("/:id", deleteLabel);
