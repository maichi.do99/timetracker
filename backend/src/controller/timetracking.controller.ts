import { RequestHandler } from "express";
import { Timetracking, timetrackingSchema } from "../entity/timetracking.model";
import { getRepository, IsNull, Not } from "typeorm";

class TimetrackingResult {
  timetracking?: Timetracking;
  timetrackingList?: Timetracking[];
  error?: string;
}

type FindAllTimetrackingsResponseBody = {
  timetrackingList: Timetracking[];
};

type FindTimetrackingResponseBody = {
  timetracking?: Timetracking;
  error?: string;
};

type SaveTimetrackingRequestBody = {
  timetracking: Timetracking;
};

type SaveTimetrackingResponseBody = {
  savedTimetracking?: Timetracking;
  error?: string;
};

export const findAllTimetrackings: RequestHandler<
  void,
  FindAllTimetrackingsResponseBody
> = async (req, res) => {
  const repository = getRepository(Timetracking);
  const result = await repository.find({ relations: ["task"] });
  res.send({ timetrackingList: result });
};

export const findAllByTaskId: RequestHandler<
  { id: string },
  FindAllTimetrackingsResponseBody
> = async (req, res) => {
  const repository = getRepository(Timetracking);
  const result = await repository.find({
    where: { task: { id: req.params.id } },
    relations: ["task"],
  });
  res.send({ timetrackingList: result });
};

export const findAllRunning: RequestHandler<
  void,
  FindAllTimetrackingsResponseBody
> = async (req, res) => {
  const repository = getRepository(Timetracking);
  const result = await repository.find({
    where: {
      startTime: Not(IsNull()),
      endTime: IsNull(),
    },
    relations: ["task"],
  });

  res.send({ timetrackingList: result });
};

export const findTimetrackingById: RequestHandler<
  { id: string },
  FindTimetrackingResponseBody
> = async (req, res) => {
  const result = await findById(req.params.id);

  if (result.timetracking) {
    res.send({ timetracking: result.timetracking });
  } else {
    res.send({ error: result.error });
  }
};

export const saveTimetracking: RequestHandler<
  SaveTimetrackingRequestBody,
  SaveTimetrackingResponseBody
> = async (req, res) => {
  const result = await save(req.body.timetracking);
  if (result.timetracking) {
    res.send({ savedTimetracking: result.timetracking });
  } else {
    res.send({ error: result.error });
  }
};

export const deleteTimetracking: RequestHandler<
  { id: string },
  { error?: string }
> = async (req, res) => {
  const repository = getRepository(Timetracking);
  const result = await findById(req.params.id);
  if (result.timetracking) {
    await repository.delete(req.params.id);
    res.status(204).send();
  } else {
    res.send({ error: result.error });
  }
};

export const startTimetracking: RequestHandler<
  { id: string },
  SaveTimetrackingResponseBody
> = async (req, res) => {
  const result = await start(req.params.id);
  if (result.timetracking) {
    res.send({ savedTimetracking: result.timetracking });
  } else {
    res.send({ error: result.error });
  }
};

export const stopTimetracking: RequestHandler<
  { id: string },
  SaveTimetrackingResponseBody
> = async (req, res) => {
  const result = await stop(req.params.id);
  if (result.timetracking) {
    res.send({ savedTimetracking: result.timetracking });
  } else {
    res.send({ error: result.error });
  }
};

async function start(id: string): Promise<TimetrackingResult> {
  const findResult = await findById(id);
  const timetracking = findResult.timetracking;

  let result = new TimetrackingResult();
  if (timetracking) {
    timetracking.startTime = new Date();
    result = await save(timetracking);
  } else {
    result.error = findResult.error;
  }

  return result;
}

async function stop(id: string): Promise<TimetrackingResult> {
  const findResult = await findById(id);
  const timetracking = findResult.timetracking;

  let result = new TimetrackingResult();
  if (timetracking) {
    timetracking.endTime = new Date();
    result = await save(timetracking);
  } else {
    result.error = findResult.error;
  }

  return result;
}

async function findById(id: string): Promise<TimetrackingResult> {
  const repository = getRepository(Timetracking);
  const result = new TimetrackingResult();

  const timetracking = await repository.findOne(id, { relations: ["task"] });

  if (timetracking) {
    result.timetracking = timetracking;
  } else {
    result.error = "Entity not found, given Id: " + id;
  }

  return result;
}

async function save(timetracking: Timetracking): Promise<TimetrackingResult> {
  const repository = getRepository(Timetracking);
  const isOk = timetrackingSchema.isValidSync(timetracking);

  const result = new TimetrackingResult();
  if (isOk) {
    result.timetracking = await repository.save(timetracking);
  } else {
    result.error = "Validation failed.";
  }
  return result;
}
