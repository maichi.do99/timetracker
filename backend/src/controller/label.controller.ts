import { RequestHandler } from "express";
import { Label, labelSchema } from "../entity/label.model";
import { getRepository } from "typeorm";
import { CommonUtils } from "../util/CommonUtils";
import { deleteLabelList, findById as findTaskById } from "./task.controller";

class LabelResult {
  label?: Label;
  labelList?: Label[];
  error?: string;
}

type FindAllLabelsResponseBody = {
  labelList?: Label[];
  error?: string;
};

type FindLabelResponseBody = {
  label?: Label;
  error?: string;
};

type SaveLabelRequestBody = {
  label: Label;
};

type SaveLabelResponseBody = {
  savedLabel?: Label;
  error?: string;
};

export const findAllLabels: RequestHandler<void, FindAllLabelsResponseBody> =
  async (req, res) => {
    const repository = getRepository(Label);
    const result = await repository.find({
      relations: ["taskList"],
    });
    res.send({ labelList: result });
  };

export const findLabelById: RequestHandler<
  { id: string },
  FindLabelResponseBody
> = async (req, res) => {
  const result = await findById(req.params.id);

  if (result.label) {
    res.send({ label: result.label });
  } else {
    res.send({ error: result.error });
  }
};

export const saveLabel: RequestHandler<
  SaveLabelRequestBody,
  SaveLabelResponseBody
> = async (req, res) => {
  const result = await save(req.body.label);
  if (result.label) {
    res.send({ savedLabel: result.label });
  } else {
    res.send({ error: result.error });
  }
};

export const deleteLabel: RequestHandler<
  { id: string },
  void | { error: string }
> = async (req, res) => {
  const findResult = await findById(req.params.id);
  const label = findResult.label;

  if (label) {
    await deleteLabelFromTasksAndLabel(label);
    res.status(204).send();
  } else {
    res.send({ error: "Entity not found, given Id: " + req.params.id });
  }
};

async function findById(id: string): Promise<LabelResult> {
  const repository = getRepository(Label);
  const result = new LabelResult();

  const label = await repository.findOne(id, {
    relations: ["taskList"],
  });

  if (label) {
    result.label = label;
  } else {
    result.error = "Entity not found, given Id: " + id;
  }

  return result;
}

async function save(label: Label): Promise<LabelResult> {
  const repository = getRepository(Label);
  const isOk = labelSchema.isValidSync(label);

  const result = new LabelResult();
  if (isOk) {
    result.label = await repository.save(label);
  } else {
    result.error = "Validation failed.";
  }

  return result;
}

async function deleteLabelFromTasksAndLabel(label: Label) {
  const repository = getRepository(Label);
  if (CommonUtils.listNotEmpty(label.taskList)) {
    for (const task of label.taskList) {
      const findTaskResult = await findTaskById(task.id);
      if (findTaskResult.task) {
        await deleteLabelList(findTaskResult.task, [label]);
      }
    }
  }

  await repository.delete(label.id);
}
