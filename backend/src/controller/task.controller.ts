import { RequestHandler } from "express";
import { Task, taskSchema } from "../entity/task.model";
import { getManager, getRepository } from "typeorm";
import { Label } from "../entity/label.model";

class TaskResult {
  task?: Task;
  taskList?: Task[];
  error?: string;
}

type FindAllTasksResponseBody = {
  taskList?: Task[];
  error?: string;
};

type FindAllTasksByLabelIdResponseBody = {
  taskList?: Task[];
  error?: string;
};

type FindTaskResponseBody = {
  task?: Task;
  error?: string;
};

type SaveTaskRequestBody = {
  task: Task;
};

type SaveTaskResponseBody = {
  savedTask?: Task;
  error?: string;
};

type ChangeLabelListRequestBody = {
  labelList: Label[];
};

type ChangeLabelListResponseBody = {
  updatedTask?: Task;
  error?: string;
};

export const findAllTasks: RequestHandler<void, FindAllTasksResponseBody> =
  async (req, res) => {
    const repository = getRepository(Task);
    const result = await repository.find({
      relations: ["labelList", "timetrackingList"],
    });
    res.send({ taskList: result });
  };

export const findAllByLabelId: RequestHandler<
  { labelId: string },
  FindAllTasksByLabelIdResponseBody
> = async (req, res) => {
  const result = await getTaskListByLabelId(req.params.labelId);
  if (result.taskList) {
    res.send({ taskList: result.taskList });
  } else {
    res.send({ error: result.error });
  }
};

export const findTaskById: RequestHandler<
  { id: string },
  FindTaskResponseBody
> = async (req, res) => {
  const result = await findById(req.params.id);

  if (result.task) {
    res.send({ task: result.task });
  } else {
    res.send({ error: result.error });
  }
};

export const saveTask: RequestHandler<
  SaveTaskRequestBody,
  SaveTaskResponseBody
> = async (req, res) => {
  const result = await save(req.body.task);
  if (result.task) {
    res.send({ savedTask: result.task });
  } else {
    res.send({ error: result.error });
  }
};

export const deleteTask: RequestHandler<{ id: string }, { error?: string }> =
  async (req, res) => {
    const repository = getRepository(Task);
    const result = await findById(req.params.id);
    if (result.task) {
      await repository.delete(req.params.id);
      res.status(204).send();
    } else {
      res.send({ error: result.error });
    }
  };

export const addLabelListToTask: RequestHandler<
  { id: string },
  ChangeLabelListResponseBody,
  ChangeLabelListRequestBody
> = async (req, res) => {
  const findResult = await findById(req.params.id);

  if (findResult.task) {
    const result = await addLabelList(findResult.task, req.body.labelList);
    if (result.task) {
      res.send({ updatedTask: result.task });
    } else {
      res.send({ error: result.error });
    }
  } else {
    res.status(404).send({ error: findResult.error });
  }
};

export const deleteLabelListFromTask: RequestHandler<
  { id: string },
  ChangeLabelListResponseBody | { error: string },
  ChangeLabelListRequestBody
> = async (req, res) => {
  const findResult = await findById(req.params.id);

  if (findResult.task) {
    const result = await deleteLabelList(findResult.task, req.body.labelList);
    if (result.task) {
      res.send({ updatedTask: result.task });
    } else {
      res.send({ error: result.error });
    }
  } else {
    res.status(404).send({ error: findResult.error });
  }
};

async function getTaskListByLabelId(labelId: string): Promise<TaskResult> {
  const result = new TaskResult();

  const entityManager = getManager();
  const query = await entityManager
    .query(
      "SELECT task.name, task.description FROM task_label_list_label JOIN task WHERE task_label_list_label.labelId LIKE ?",
      [labelId]
    )
    .catch((e) => (result.error = "A SQL Query error occured: " + e));

  result.taskList = getResultFromQuery(query);

  return result;
}

function getResultFromQuery(query: any): Task[] {
  const taskList: Task[] = [];
  for (const row of query) {
    const task = Task.create({
      name: row.name,
      description: row.description,
    });
    taskList.push(task);
  }

  return taskList;
}

export async function findById(id: string): Promise<TaskResult> {
  const repository = getRepository(Task);
  const result: TaskResult = new TaskResult();

  const task = await repository.findOne(id, {
    relations: ["labelList"],
  });

  if (task) {
    result.task = task;
  } else {
    result.error = "Entity not found, given Id: " + id;
  }

  return result;
}

async function save(task: Task): Promise<TaskResult> {
  const repository = getRepository(Task);
  const isOk = taskSchema.isValidSync(task);

  const result = new TaskResult();
  if (isOk) {
    result.task = await repository.save(task);
  } else {
    result.error = "Validation failed.";
  }

  return result;
}

async function addLabelList(
  task: Task,
  labelList: Label[]
): Promise<TaskResult> {
  const updateTask = task;
  updateTask.labelList = labelList;
  return save(updateTask);
}

export async function deleteLabelList(
  task: Task,
  labelList: Label[]
): Promise<TaskResult> {
  const updateTask = task;
  updateTask.labelList = updateTask.labelList.filter(
    (l) => !labelList.map((label) => label.id).includes(l.id)
  );
  return save(updateTask);
}
