export class CommonUtils {
  static stringIsEmpty(s: string): boolean {
    return !s || s.length == 0;
  }

  static listIsEmpty(l: any[]): boolean {
    return !l || l.length == 0;
  }

  static listNotEmpty(l: any[]): boolean {
    return l && l.length > 0;
  }
}
