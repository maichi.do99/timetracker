import { Column, Entity, ManyToMany } from "typeorm";
import * as yup from "yup";
import { AbstractEntity } from "./abstractentity.model";
import { Task } from "./task.model";

export const labelSchema = yup.object().shape({
  name: yup.string().required(),
});

@Entity()
export class Label extends AbstractEntity {
  @Column()
  name!: string;

  @ManyToMany((type) => Task, (task) => task.labelList)
  taskList!: Task[];
}
