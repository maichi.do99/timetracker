import { Column, Entity, ManyToOne } from "typeorm";
import * as yup from "yup";
import { AbstractEntity } from "./abstractentity.model";
import { Task } from "./task.model";

export const timetrackingSchema = yup.object().shape({
  description: yup.string().required(),
  task: yup.object().required(),
});

@Entity()
export class Timetracking extends AbstractEntity {
  @Column()
  description!: string;

  @Column({ nullable: true })
  startTime: Date;

  @Column({ nullable: true })
  endTime: Date;

  @ManyToOne((type) => Task, (task) => task.timetrackingList, {
    cascade: true,
    onDelete: "CASCADE",
  })
  task!: Task;
}
