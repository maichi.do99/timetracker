import { Column, Entity, JoinTable, ManyToMany, OneToMany } from "typeorm";
import * as yup from "yup";
import { AbstractEntity } from "./abstractentity.model";
import { Timetracking } from "./timetracking.model";
import { Label } from "./label.model";

export const taskSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

export type TaskInitProps = {
  name: string;
  description: string;
};

@Entity()
export class Task extends AbstractEntity {
  @Column()
  name!: string;

  @Column()
  description!: string;

  @ManyToMany((type) => Label, (label) => label.taskList, {
    cascade: true,
    onDelete: "CASCADE",
  })
  @JoinTable()
  labelList!: Label[];

  @OneToMany((type) => Timetracking, (timetracking) => timetracking.task)
  timetrackingList!: Timetracking[];

  static create(props: TaskInitProps): Task {
    const task = new Task();
    task.name = props.name;
    task.description = props.description;
    return task;
  }
}
