# Track It! - Dein Timetracker (Express Backend)

Mit Track it! wird das tracken von Aufgaben und Tasks kinderleicht gemacht. Lege einen Task an und du kannst sofort dein erstes Tracking starten!
Es handelt sich hierbei um das Backend, das die dahinterliegende Logik für Datenbank, Persistenz und Co. beinhaltet.

## Genutzte Technologien & Requirements

- NodeJs
- Express (Typescript)
- Typeorm
- Yup
- Docker

## Funktionalität

Hierüber kann das React Frontend über HTTP Requests Tasks, Labels und Timetrackings erstellen, updaten, abrufen und löschen. Daten werden so direkt in der MySQL Datenbank persistiert. Über den Router werden die einzelnen Routen korrekt mit den Controllercalls gemapped, sodass eine einfache Verbindung mit dem Frontend ermöglicht wird. Jeder Call wird ebenfalls validiert, sodass keine fehlerhaften Daten in der Datenbank abgelegt werden können.

## Routenstruktur
Es existieren vier Router, wovon drei von dem GlobalRouter abhängig sind. 

![img_1.png](img_1.png)

## Setup

1. **Clone** das **Repository** mithilfe von Git.
2. **Öffne** das Projekt in einer beliebigen IDE. Bevorzugt **IntelliJ**, da das Setup sich daran anlehnt. Solltest du jedoch kein IntelliJ verwenden, müssen noch einige Config-Dateien abgeändert werden, um einen Start des Front- und Backends über Docker ermöglichen zu können.
3. Starte das **Docker-Compose** File unter dem Hauptordner.
4. **Installiere** die **Dependencies** für die jeweiligen beiden Node Projekte unter den Ordnern "backend" und "frontend" mithilfe des Befehls "**npm install**".
5. Füge die **Datenbank zu IntelliJ** hinzu: (Achtung! Achte darauf, dass die Datenbank unter Docker gestartet wurde)
   5.1. Öffne auf der rechten Seite den Tab "Database".
   5.2. Füge eine neue Datenbank hinzu und wähle als Datasource die Option "MySQL" aus.
   5.3. Der Host lautet "localhost", der Port "3306". User und Passwort findest du in der ormconfig.js Datei unter dem Punkt "config".
   5.4. Nachdem du die Treiber heruntergeladen hast, kannst du die Verbindung zur Datenbank testen.
6. Führe das Skript **"schema:sync" in der backend/package.json** aus, um das Datenbank Schema zu synchronisieren.
7. Um nun das Backend zu starten, starte das **"start" Skript in der backend/package.json**.
9. Nun können HTTP Requests über die Adresse "**http://localhost:3000**" angenommen werden.

## Testen der Anwendung

Folgende Funktionalitäten können im Frontend getestet werden:

Tasks:
- Finden aller Tasks
- Finden eines Tasks nach Id
- Finden aller Tasks nach einer LabelId
- Speichern eines Tasks (create & update)
- Hinzufügen einer LabelList zu einem Task
- Entfernen einer LabelList von einem Task
- Löschen eines Tasks

Timetrackings
- Finden aller Timetrackings
- Finden eines Timetrackings nach Id
- Finden aller Timetrackings nach einer TaskId
- Finden aller laufenden Timetrackings
- Speichern eines Timetrackings (create & update)
- Starten & Beenden eines Timetrackings
- Löschen eines Timetrackings

Labels

- Finden aller Labels
- Finden eines Labels nach Id
- Speichern eines Labels (create & update)
- Löschen eines Labels

Zu allen Funktionalitäten kann ebenfalls ein nicht erfolgreiches Speichern getestet werden. Der Entwickler wird dann in der Konsole darauf aufmerksam gemacht.